﻿using UnityEngine;
using System.Collections;


public class DestroyByContact : MonoBehaviour 
{

	public GameObject explosion;
	public GameObject playerExplosion;
	public int scoreValue;
	private GameController gameController;


	void Start()
	{ 
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null)
						gameController = gameControllerObject.GetComponent <GameController> ();

		if (gameController == null)
			Debug.Log ("Cannot find 'GameController' script");
	}
	/**
	 * \brief : Gestion des collisions
	 */
	void OnTriggerEnter(Collider other)
	{
		

		// Contact astéroid / Limites : on ignore -> cf DestroyByBoundary
		if (other.tag == "Boundary")
			return;

		// Contact entre ennemi et astéroid ignoré
		if (gameObject.tag == "Ennemy" && other.tag == "Ennemy")
				return;
		Debug.Log ("Tag : " + other.tag + " - " + gameObject.tag );
		// Contact avec le bouclier. 
		if (other.tag == "Shield") 
		{
			// Gestion des effets d'explosion
			Instantiate (explosion, transform.position, transform.rotation);
			gameController.AddScore (scoreValue);

			Destroy(gameObject);
			return;
		}

		// Gestion des effets d'explosion
		Instantiate (explosion, transform.position, transform.rotation);

		// Explosion spéciale avec le joueur
		if (other.tag == "Player") 
		{
			Instantiate (playerExplosion, other.transform.position, other.transform.rotation);
			gameController.GameOver();		
		}
		// Ajout du score pour chaque objet détruit
		gameController.AddScore (scoreValue);


		// Destruction des objets
		Destroy (other.gameObject);
		Destroy (gameObject);
	}
} 
