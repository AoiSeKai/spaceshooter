﻿using UnityEngine;
using System.Collections;

// pour les astéroides
[System.Serializable]
public class Hazard
{
	public GameObject hazard; 		// L'ennemi
	public int hazardCount;			// Le nombre d'ennemi popant à chaque vague
}

// pour les vaisseaux ennemis
[System.Serializable]
public class EnnemyShip
{
	public GameObject ennemyShip;  // L'ennemi
	public int shipCount;			// Le nombre d'ennemi à chaque vague

}
[System.Serializable]
public class Wave
{
	public Vector3 spawnValues;		// La position d'apparition des ennemis
	public float spawnWait;			// Temps d'attente avant un spawn
	public float startWait;			// Temps avant la toute première vague
	public float waveWait;			// Temps d'attente entre chaque spawn
}


public class GameController : MonoBehaviour 
{
	public Hazard hazard;
	public Wave wave;
	public EnnemyShip ennemy;

	// Pour le score
	public GUIText scoreText;
	private int score;

	public GUIText gameOverText;
	private bool gameOver;
	
	public GUIText restartText;
	private bool restart;

	void Start ()
	{
		// Mis à jour du score
		score = 0;
		UpdateScore ();

		gameOver = false;
		restart = false;

		restartText.text = "";
		gameOverText.text = "";

		// Apparition des vagues d'ennemis
		StartCoroutine(SpawnWaves ());
	}

	void Update()
	{
		// On reload le niveau actuel :)
		if (restart)
				if (Input.GetKeyDown (KeyCode.R))
						Application.LoadLevel (Application.loadedLevel);

	}

	/**
	 * \brief : Fait apparaitre des vagues d'ennemi en boucle. 
	 * 			Le nombre d'ennemi par vague est défini dans l'interface
	 * 			Le temps d'attente entre chaque vague également
	 */
	IEnumerator SpawnWaves()
	{
		yield return new WaitForSeconds (wave.startWait);
		int cptEnnemy, cptHazard, rand;
		while(true)
		{
			cptEnnemy = 0;
			cptHazard = 0;
			for (int i = 0; i < hazard.hazardCount + ennemy.shipCount; i++) 
			{
				Vector3 spawnPosition = new Vector3 (Random.Range (-wave.spawnValues.x, wave.spawnValues.x), 
				                                     wave.spawnValues.y, 
				                                     wave.spawnValues.z
				);
				Quaternion spawnRotation = Quaternion.identity;

				// On fait apparaitre soit un astéroid soit un ennemi 
				if(cptEnnemy == ennemy.shipCount) 			// on a atteint le nombre d'ennemi max
					Instantiate (hazard.hazard, spawnPosition, spawnRotation);
				else if(cptHazard == hazard.hazardCount)		// on a atteint le nb astéroides max
					Instantiate (ennemy.ennemyShip, spawnPosition, spawnRotation);
				else // random 
				{
					rand = Random.Range(0, 2);
					if(rand == 0) // Astéroid si random = 0 
					{
						Instantiate (hazard.hazard, spawnPosition, spawnRotation);
						cptHazard++;
					}
					else
					{
						Instantiate (ennemy.ennemyShip, spawnPosition, spawnRotation);
						cptEnnemy++;
					}
				}
				yield return new WaitForSeconds (wave.spawnWait);
			}
			yield return new WaitForSeconds(wave.waveWait);

			if(gameOver)
			{
				restartText.text = "Press 'R' for Restart";
				restart = true;
				break;
			}
		}
	}

	/**
	 * \brief : A chaque fois qu'un ennemi/astéroid est détruit, 
	 * 			Appel de cette fonction pour mettre à jour le score
	 */
	public void AddScore(int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore ();
	}

	/**
 * \brief Met à jour le score sur la vue
	 */
	void UpdateScore()
	{
		scoreText.text = "Score: " + score;
	}

	public void GameOver()
	{
		gameOverText.text = "Game Over!";
		gameOver = true;
	}


}
