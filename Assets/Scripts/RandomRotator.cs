﻿using UnityEngine;
using System.Collections;

public class RandomRotator : MonoBehaviour {

	public float tumble;

	void Start()
	{
		// Vitesse de rotation 
		rigidbody.angularVelocity = Random.insideUnitSphere * tumble; 
	}
}
