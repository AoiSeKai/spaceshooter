﻿using UnityEngine;
using System.Collections;

public class ShieldController : MonoBehaviour 
{
	public GameObject shieldLife; // Durée de vie du bouclier
	public GameObject shield;	  // Le bouclier
	public int TimeNextShield;	  // Lorsque le boubou est vide, il faut attendre un moment pour le régénérer

	private float positionChange = 0.0015f; // Pour modifier la position en x de la barre (qu'elle reste toujours au meme point de départ)
	private float scaleChange = 0.005f;		// Pour changer la taille de la barre de vie
	private float scaleMax;			// La taille max du bouclier (sauf si bonus?)
	private float positionMax;		// Position de la barre en x

	private bool increase;			// increase shield or not
	private bool cooldown;			// Si le bouclier a été utilisé, il devient inutilisable pendant 'TimeNextShield' sec
	private float cooldownStart;

	void Start()
	{
		DeActivateShield ();

		cooldown = false;
		increase = true;
		positionMax = shieldLife.transform.position.x;
		scaleMax = shieldLife.transform.localScale.x;
	}

	void FixedUpdate()
	{
		if (cooldown) 
		{
			Debug.Log("Time : " + (Time.time - cooldownStart) + ", " + TimeNextShield);
			if(Time.time - cooldownStart > TimeNextShield)
			{
				cooldown = false;
				RestoreShield();
			}
		}
		else
		{
			if (Input.GetKeyDown(KeyCode.LeftShift) || increase == false)
				DecreaseLife();
			if (Input.GetKeyUp (KeyCode.LeftShift) || increase == true)
				IncreaseLife();
		}
	}

	void ActivateShield()
	{
		shield.renderer.enabled = true;
		shield.collider.enabled = true;
	}

	void DeActivateShield()
	{
		shield.renderer.enabled = false;
		shield.collider.enabled = false;
	}

	void RestoreShield()
	{
		Vector3 scaling = shieldLife.transform.localScale;
		scaling.x = scaleMax;
		shieldLife.transform.localScale = scaling;

		Vector3 pos = shieldLife.transform.position;
		pos.x = positionMax;
		shieldLife.transform.position = pos;
	}

	/**
	 * \brief : Diminue la vie du bouclier
	 */
	void DecreaseLife()
	{
		ActivateShield ();

		Vector3 scaling = shieldLife.transform.localScale;
		scaling.x -= scaleChange;

		// Désactivation du bouclier
		if (scaling.x < 0) 
		{
			scaling.x = 0;
			cooldown = true;
			cooldownStart = Time.time;
			DeActivateShield();
		}

		shieldLife.transform.localScale = scaling;


		Vector3 pos = shieldLife.transform.position;
		pos.x -= positionChange;
		if (pos.x > positionMax) pos.x = positionMax;
		
		shieldLife.transform.position = pos;
		increase = false;
	}

	/**
	 * \brief : Diminue la vie du bouclier
	 */
	void IncreaseLife()
	{
		DeActivateShield ();

		Vector3 scaling = shieldLife.transform.localScale;
		scaling.x += scaleChange;
		if (scaling.x > scaleMax) scaling.x = scaleMax;
		shieldLife.transform.localScale = scaling;
		
		Vector3 pos = shieldLife.transform.position;
		pos.x += positionChange;
		if (scaling.x >= scaleMax) pos.x = positionMax;
		
		shieldLife.transform.position = pos;
		increase = true;
	}
}
