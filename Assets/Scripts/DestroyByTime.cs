﻿using UnityEngine;
using System.Collections;

public class DestroyByTime : MonoBehaviour 
{
	public float lifetime; // Durée de vie de l'objet

	void Start()
	{
		Destroy (gameObject, lifetime);
	}
}
