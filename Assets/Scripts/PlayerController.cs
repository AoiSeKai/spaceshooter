﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary
{
	public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour {

	public float speed;
	public float tilt;
	public Boundary boundary;

	public bool ModeMouse;

	public GameObject shot;
	public Transform shotSpawn;

	public float fireRate;

	private float nextFire;

	void Update()
	{
		if (ModeMouse == true) 
		{
			if (Input.GetButton ("Fire1") && Time.time > nextFire) 
		    {
				Shoot();
			}		
		} 
		else 
		{
			if (Input.GetKey("space") && Time.time > nextFire) 
			{
				Shoot();
			}
		}
	}

	void Shoot()
	{
		nextFire = Time.time + fireRate;
		Instantiate (shot, shotSpawn.position, shotSpawn.rotation);  
		
		// Partie audio
		audio.Play ();
	}

	void FixedUpdate()
	{
		// Avec le clavier
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
	
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

		/*Vector3 inputPosition = Input.mousePosition;
		Debug.Log ("Position : " + Input.mousePosition.x + ", " + Input.mousePosition.y + ", " + Input.mousePosition.z);
		Vector3 ray = Camera.main.ScreenToWorldPoint(new Vector3(inputPosition.x, 
		                                                         inputPosition.y, 
		                                                         Camera.main.transform.position.z - transform.position.y));
		movement = ray;
		speed = 3;
*/

		rigidbody.velocity = movement * speed;
		rigidbody.position = new Vector3
		(
				Mathf.Clamp(rigidbody.position.x, boundary.xMin, boundary.xMax),
				0.0f,
				Mathf.Clamp(rigidbody.position.z, boundary.zMin, boundary.zMax)
		);

		rigidbody.rotation = Quaternion.Euler (0.0f, 0.0f, rigidbody.velocity.x * -tilt);
	}
}
