﻿using UnityEngine;
using System.Collections;

public class EnnemyController : MonoBehaviour {

	public Transform shotSpawn; // Position du tir
	public GameObject shot;
	
	public float fireRate;		// cadence de tir
		
	private float nextFire;		// prochain moment où l'on peut tirer

	void Start()
	{	
		// Pour qu'il soit en face
		transform.Rotate (0.0f, -180.0f, 0.0f);

		// 1er tir 
		nextFire = Time.time + 1;
	}

	void Update()
	{
		// Tir dès que possible
		if (Time.time > nextFire) 
		{
			nextFire = Time.time + fireRate;
			Instantiate (shot, shotSpawn.position, shotSpawn.rotation);  
			
			// Partie audio
			audio.Play ();
		}
	}
}
